

## Q1: What controversies do you see in the following API example?

User object API doc

    GET /users
    POST /users/new
    POST /users/:id/update
    POST /users/:id/rename
    POST /users/:id/update-timezone
    DELETE /users/delete?id=:id

Here are some examples of the behavior:    

    POST /users/new HTTP/1.1
    {
    "name": "Cthulhu"
    }

    HTTP/1.1 200
    "Error: Username already exists"

Answers:

- ``POST /users/new`` returned error but with 200-s code -- I'd return 400-s error code.
- ``POST /users/new``: here the ``/new`` is not needed
- ``POST /users/:id/update``: here the ``PUT /users/:id`` should be used instead (but only with full object provided)
- rename, update-timezone should be chaged to ``PATCH /users/:id`` with partial update of needed fields provided
- I'd prefer to have ``DELETE /users/delete/:id`` for the last one in order to simplify API usage on frontend.

## Q2: What issues do you see in the following CSS file?

    @import 'fonts.css';

    .header {
      font-size: 20px;
      color: #ccaadd;
    }
    .header.large {
      font-size: 24px !important;
      color: #ccaadd;
    }
    .lfnt {
      font-weight: 500;
      font-size: 30px;
    }
    div > a > span > .blue {
      color: #00f;
    }
    .box {
      margin-top: 3;
      margin-bottom: 3;
      margin-left: 3;
      margin-right: 3;
    }

Answers:

- .header.large font-size -- ``!important`` is not needed, the rule already has a priority
- ``.lfnt`` -- it's not critical, but the class name should have more self-explanatory name.
- ``div > a > span > .blue`` -- will be very slow, maybe some special className & 1-level selector should be used for such a special case.
- ``.box { margin: 3 }`` will do the same with less code

## Q3: What problems related to password security can you see in the following example?

A web service stores user information in a database and uses passwords for
authentication. Here's how the user password storing and authentication is
implemented in ruby (the actual data storage and retrieval is outside the scope of
the example):

    require 'digest'

    class User

      # Use salted passwords
      PASSWORD_SALT="trustno1"

      # Stored password hash will be accessible through user.hashed_password
      attr_accessor :hashed_password

      # Authenticates user against given password and returns true
      # the password matches the stored one
      def verify_password(password)
        if hashed_password.nil? || password.nil?
          false
        else
          User.hash_password(password) == hashed_password
        end
      end

      # Changes user's password
      def change_password(new_password)
        self.hashed_password = User.hash_password(new_password.to_s)
      end

      # Hashes the input with salt
      def self.hash_password(password)
        Digest::MD5.hexdigest(password + PASSWORD_SALT)
      end
    end

Answers: 

- First of all, I'd move hardcoded PASSWORD_SALT constant out of the code (maybe environment variable getted or something like that).
- Change password method may have one more parameter: old_password, that will be verified before setting a new one.

## Q4: What would you give as a feedback
for a pull request including this code?

    function getDepositHistorySum(user) {
      const deposits = user.transactions.history.deposits;
      let sum = 0;
      for (var i = 0; i < deposits.length; i += 1) {
        sum += deposits[i].amount;
      }
      return sum;
    }

Answers:

- ``user.transactions.history.deposits`` -- all the properties & user himself can be undefined, so it'll break the selector.
- ``for`` can be changed to a function like Array.prototype.reduce to get rid of extra code & variables.

## Q5: What issues do you see in this JavaScript function?

    async function getConversations() {
      u = 'https://example.com/api';
      var convsPath = '/conversations';
      let msgsPath = '/messages';
      
      const result = await fetch(u + convsPath)
        .then(response => response.json())
        .then(conversations => {
      
        let promises = conversations.map(item => {
          const messagesUrl = u + convsPath + '/' + item.id + msgsPath;
          
          return fetch(messagesUrl)
            .then(response => {
              return response.json().then(msgs => {
                item.messages = msgs;
                return item;
              });
            });
          });

        return Promise.all(promises);
      });

      return result;
    }

Answers:

- I would never pass that within code review with such promise chains :)
- u is defined in global scope & actually, u, convsPath, msgsPath should be constants as soon as the're not changed anywhere
- await without try/catch used here, as soon as no errors are handled at all - that should be re-factored at least to bubble errors up.
- the host url ``https://example.com/api`` could be externalized into env variable of similar.


## Q6: What would you give as a feedback for a pull request including this code?

    Account.prototype.increaseBalance = function(amount, isCredit) {
      if (!isCredit) {
        this.debitBalance += amount;
      } else {
        this.creditBalance += amount;
      }
    };

Answers:

- ``debitBalance`` & ``creditBalance`` properties should be initialized (at least by a default value) at the moment of calling the function
