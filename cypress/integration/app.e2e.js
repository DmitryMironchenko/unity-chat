describe('Simple sanity test', function() {
  it('Open chat', function() {
    cy.visit('http://localhost:3000')

    cy.get('span').should('have.text', 'Conversations')
    cy.get('p').should('have.text', 'Please select the conversation in the sidebar.')

    cy.wait(2000)
    cy.get('#root > div > div > div > ul > div:nth-child(1)').click()

    cy.url().should('include', '/conversations/1')

    // Crazy approach to have such a selector
    cy.get(
      '#root > div > main > ul > li > div.MuiListItemText-root.makeStyles-messageBody-117 > span'
    ).should('have.text', 'Moi!')
  })
})
