import { createStyles, makeStyles } from '@material-ui/core'
import React from 'react'
import { Redirect, Route, Switch } from 'react-router'

import './App.css'
import Chat from './components/chat'

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
    },
  })
)

const AppRouter: React.FC = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Switch>
        <Route exact={true} path="/conversations/:id?" component={Chat} />
        <Redirect to="/conversations/" />
      </Switch>
    </div>
  )
  return null
}

export default AppRouter
