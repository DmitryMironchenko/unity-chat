import { State as ChatState } from './components/ducks/types'

export interface User {
  id: number
  username: string
  avatar_url: string
}

export type ConversationWithUserData = Conversation & {
  with_user: User
}

export interface ConversationMessage {
  id: number
  conversation_id: number
  body: string
  from_user_id: number
  created_at: string
}

export type ConversationMessageWithUserData = ConversationMessage & {
  from_user: User
}

export interface Conversation {
  id: number
  with_user_id: number
  unread_message_count: number
}

export interface RootState {
  chat: ChatState
}
