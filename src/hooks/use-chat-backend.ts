import _ from 'lodash'
import { useEffect, useState } from 'react'

import {
  Conversation,
  ConversationMessage,
  ConversationMessageWithUserData,
  ConversationWithUserData,
  User,
} from '../types'

export interface IHttpResponse<T> extends Response {
  parsedBody?: T
}

const useHttp = <T>(request: RequestInfo) => {
  const [data, setData] = useState<T | null>(null)

  // TODO: merge status state varibles into master one
  const [isSuccess, setIsSuccess] = useState<boolean>()
  const [isPending, setIsPending] = useState<boolean>()
  const [error, setError] = useState<string>()

  const getData = async () => {
    try {
      setIsPending(true)

      const response: IHttpResponse<T> = await fetch(request)
      const rawData = await response.json()

      setData(rawData)
      setIsPending(false)
      setIsSuccess(true)
    } catch (e) {
      setIsPending(false)
      setIsSuccess(false)
      setError(e.message)
    }
  }

  return { data, getData, isPending, isSuccess, error }
}

export const useUsers = () => useHttp<User[]>(`${process.env.REACT_APP_API_URL}/users`)

export const useConversationsWithUserData = () => {
  const [status, setStatus] = useState<{
    data: ConversationWithUserData[]
    isPending: boolean
    isSuccess: boolean
    error: string | null
  }>({
    data: [],
    isPending: false,
    isSuccess: false,
    error: null,
  })
  const users = useUsers()
  const converstions = useConversations()

  const getData = () => {
    users.getData()
    converstions.getData()
  }

  useEffect(() => {
    if (users.isSuccess && converstions.isSuccess) {
      setStatus({
        data: mergeConversationsWithUsers(converstions.data || [], users.data || []),
        isSuccess: true,
        isPending: false,
        error: null,
      })
    }

    if (users.isPending || converstions.isPending) {
      setStatus({ ...status, isPending: true })
    }

    // TODO: watch for errors
  }, [
    users.data,
    users.isSuccess,
    users.isPending,
    converstions.data,
    converstions.isSuccess,
    converstions.isPending,
  ])

  return { ...status, getData }
}

export const useConversationByIdWithUsers = (conversationId: number) => {
  const conversationMessages = useConversationMessages(conversationId)
  const users = useUsers()

  const [status, setStatus] = useState<{
    data: ConversationMessageWithUserData[]
    isPending: boolean
    isSuccess: boolean
    error: string | null
  }>({
    data: [],
    isPending: false,
    isSuccess: false,
    error: null,
  })

  const getData = () => {
    users.getData()
    conversationMessages.getData()
  }

  useEffect(() => {
    if (users.isSuccess && conversationMessages.isSuccess) {
      setStatus({
        data: _.sortBy(
          mergeConversationMessagesWithUserData(conversationMessages.data || [], users.data || []),
          datum => new Date(datum.created_at)
        ),
        isSuccess: true,
        isPending: false,
        error: null,
      })
    }

    if (users.isPending || conversationMessages.isPending) {
      setStatus({ ...status, isPending: true })
    }

    // TODO: watch for errors
  }, [
    users.data,
    users.isSuccess,
    users.isPending,
    conversationMessages.data,
    conversationMessages.isSuccess,
    conversationMessages.isPending,
  ])

  return { ...status, getData }
}

export const useConversations = () =>
  useHttp<Conversation[]>(`${process.env.REACT_APP_API_URL}/conversations`)

export const useUserById = (userId: number) => useHttp<User>(`/users/${userId}`)

export const useConversationById = (conversationId: number) =>
  useHttp<Conversation>(`${process.env.REACT_APP_API_URL}/conversations/${conversationId}`)

export const useConversationMessages = (conversationId: number) =>
  useHttp<ConversationMessage[]>(
    `${process.env.REACT_APP_API_URL}/conversations/${conversationId}/messages`
  )

export default {
  useUsers,
  useConversations,
  useUserById,
  useConversationById,
  useConversationByIdWithUsers,
  useConversationMessages,
}

// export for unit-testing
export function mergeConversationsWithUsers(
  conversations: Conversation[],
  users: User[]
): ConversationWithUserData[] {
  return conversations.map(conversation => ({
    ...conversation,
    with_user: users.find(user => user.id === conversation.with_user_id)!,
  }))
}

// export for unit-testing
export function mergeConversationMessagesWithUserData(
  conversationMessages: ConversationMessage[],
  users: User[]
): ConversationMessageWithUserData[] {
  return conversationMessages.map(message => ({
    ...message,
    from_user: users.find(user => user.id === message.from_user_id)!,
  }))
}
