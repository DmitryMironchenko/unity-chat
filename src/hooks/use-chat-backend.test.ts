import { Conversation, User } from '../types'

import { mergeConversationsWithUsers } from './use-chat-backend'

describe('Backend hooks utilities tests', () => {
  it('Shoud zip users with conversations correctly', () => {
    const users: User[] = [
      {
        id: 1,
        username: 'Foo',
        avatar_url: 'Foo',
      },
      {
        id: 2,
        username: 'bar',
        avatar_url: 'Bar',
      },
    ]

    const conversations: Conversation[] = [
      {
        id: 1,
        with_user_id: 2,
        unread_message_count: 50,
      },
      {
        id: 2,
        with_user_id: 50,
        unread_message_count: 50,
      },
    ]

    const result = mergeConversationsWithUsers(conversations, users)
    expect(result).toMatchSnapshot()
  })
})
