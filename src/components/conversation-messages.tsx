import Avatar from '@material-ui/core/Avatar'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { format } from 'date-fns'
import React, { useEffect } from 'react'

import { useConversationByIdWithUsers } from '../hooks/use-chat-backend'
import { ConversationWithUserData } from '../types'

interface Props {
  conversationId: number
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      // maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
    listItem: {
      flexWrap: 'wrap',
    },
    messageBody: {
      width: '100%',
      marginTop: 20,
    },
  })
)

const ConversationMessages: React.FC<Props> = ({ conversationId }) => {
  const classes = useStyles()
  const conversationMessages = useConversationByIdWithUsers(conversationId)

  useEffect(() => {
    conversationMessages.getData()
  }, [conversationId])

  return (
    <List className={classes.root}>
      {conversationMessages.isSuccess &&
        conversationMessages.data!.map(message => (
          <ListItem key={message.id} className={classes.listItem}>
            <ListItemAvatar>
              <Avatar alt={message.from_user.username} src={message.from_user.avatar_url} />
            </ListItemAvatar>
            <ListItemText
              primary={message.from_user.username}
              secondary={format(new Date(message.created_at), 'M/d/y h:mm:ss a')}
            />
            <ListItemText className={classes.messageBody} primary={message.body} />
          </ListItem>
        ))}
    </List>
  )
}

export default ConversationMessages
