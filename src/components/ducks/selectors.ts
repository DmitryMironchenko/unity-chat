import { RootState } from '../../types'

import { reducerName } from './reducers'
import { State } from './types'

export const selectCurrentConversation = (state: RootState) =>
  (state[reducerName] as State).conversation

export default {
  selectCurrentConversation,
}
