import { push } from 'connected-react-router'
import { AnyAction, Dispatch } from 'redux'

import { ConversationWithUserData } from '../../types'

import { setCurrentConversation } from './actions'

export const startConversation = (conversation: ConversationWithUserData) => (
  dispatch: Dispatch<AnyAction>
) => {
  dispatch(push(`/conversations/${conversation.id}`))
  dispatch(setCurrentConversation(conversation))
}

export default {
  startConversation,
}
