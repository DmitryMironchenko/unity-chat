import reducer from './reducers'

export { default as duckOperations } from './operations'
export { default as duckActions } from './actions'
export { default as duckTypes } from './types'
export { default as duckSelectors } from './selectors'

export { reducerName } from './reducers'
export default reducer
