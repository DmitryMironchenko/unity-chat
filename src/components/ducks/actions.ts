import { Conversation } from '../../types'

import actionTypes from './action-types'

export const setCurrentConversation = (conversation: Conversation) => ({
  type: actionTypes.SET_CURRENT_CONVERSATION,
  conversation,
})

export default {
  setCurrentConversation,
}
