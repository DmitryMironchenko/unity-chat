import { ConversationWithUserData } from '../../types'

export interface State {
  conversation: ConversationWithUserData | null
}
