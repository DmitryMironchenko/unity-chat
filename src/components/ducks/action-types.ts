enum ActionTypes {
  SET_CURRENT_CONVERSATION = 'chat/SET_CURRENT_CONVERSATION',
}

export default ActionTypes
