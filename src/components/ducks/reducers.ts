import { createReducer } from 'typesafe-actions'

import { ConversationWithUserData } from '../../types'

import actionTypes from './action-types'
import { State } from './types'

const initialState: State = {
  conversation: null,
}

export const reducerName = 'chat'

const reducer = createReducer(initialState, {
  [actionTypes.SET_CURRENT_CONVERSATION]: (
    state: State,
    action: { conversation: ConversationWithUserData }
  ) => ({
    ...state,
    conversation: action.conversation,
  }),
})

export default reducer
