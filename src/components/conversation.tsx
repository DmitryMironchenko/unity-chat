import { Theme } from '@material-ui/core'
import { createStyles, makeStyles } from '@material-ui/styles'
import React from 'react'

import { Conversation } from '../types'

import ConversationMessages from './conversation-messages'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing(3),
    },
  })
)

interface Props {
  conversation: Conversation
}

const ConversationComponent: React.FC<Props> = ({ conversation }) => {
  const classes = useStyles()

  return (
    <main className={classes.content}>
      {conversation && <ConversationMessages conversationId={conversation.id} />}
      {!conversation && <p>Conversation not found.</p>}
    </main>
  )
}

export default ConversationComponent
