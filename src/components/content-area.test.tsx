import { MuiThemeProvider } from '@material-ui/core/styles'
import { unwrap } from '@material-ui/core/test-utils'
import { mount, shallow } from 'enzyme'
import React from 'react'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'

import { Chat } from './chat'

const mockStore = configureMockStore()

describe('<Chat />', () => {
  const storeWithoutActiveChat = mockStore({
    chat: {
      conversation: null,
    },
  })

  const storeWithActiveChat = mockStore({
    chat: {
      conversation: {
        id: 1,
      },
    },
  })

  const routeParams = {
    match: {
      params: {
        id: 1,
      },
    },
    history: {},
    location: null,
  }

  const emptyRouteParams = {
    match: {
      params: {
        id: 1,
      },
    },
    StaticContext: null,
    history: {},
    location: null,
  }

  it('Should render waring "<p>Please select the conversation in the sidebar.</p>" when no chat is selected', () => {
    const wrapper = mount(
      <Provider store={storeWithoutActiveChat}>
        <MuiThemeProvider
          theme={{
            palette: {
              background: {
                default: '#eee',
              },
            },
            spacing: jest.fn(),
          }}
        >
          {/*
          // @ts-ignore*/}
          <Chat {...emptyRouteParams} />
        </MuiThemeProvider>
      </Provider>
    )

    expect(wrapper.debug()).toMatchSnapshot()
  })

  it('Should not render warning when active chat is not null. Should Render Loading... component', () => {
    const wrapper = mount(
      <Provider store={storeWithActiveChat}>
        <MuiThemeProvider
          theme={{
            palette: {
              background: {
                default: '#eee',
              },
            },
            spacing: jest.fn(),
          }}
        >
          {/*
          // @ts-ignore*/}
          <Chat {...routeParams} />
        </MuiThemeProvider>
      </Provider>
    )

    expect(wrapper.debug()).toMatchSnapshot()
  })
})
