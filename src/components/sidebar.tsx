import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import MailIcon from '@material-ui/icons/Mail'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import React, { useEffect } from 'react'

import { useConversationsWithUserData } from '../hooks/use-chat-backend'
import { ConversationWithUserData } from '../types'

import SidebarItem from './sidebar-item'

const drawerWidth = 240

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
      backgroundColor: '#ededed',
    },
    title: {
      padding: 20,
    },
  })
)

const Sidebar = () => {
  // @ts-ignore
  const classes = useStyles()
  const { data: conversations, getData } = useConversationsWithUserData()
  // const conversations = useConversations()

  useEffect(() => {
    getData()
  }, [])

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
      anchor="left"
    >
      <Typography component="span" className={classes.title}>
        Conversations
      </Typography>
      <List>
        {conversations &&
          conversations.map((conversation: ConversationWithUserData) => (
            <SidebarItem key={conversation.id} conversation={conversation} />
          ))}
      </List>
    </Drawer>
  )
}

export default Sidebar
