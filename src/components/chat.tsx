import { Theme } from '@material-ui/core'
import { Typography } from '@material-ui/core'
import { createStyles, makeStyles } from '@material-ui/styles'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RouteComponentProps } from 'react-router'

import { useConversationById } from '../hooks/use-chat-backend'
import { Conversation } from '../types'

import ConversationComponent from './conversation'
import { duckActions, duckSelectors } from './ducks'
import { withSidebar } from './hocs'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(3),
    },
  })
)

interface ConversationDataLoaderProps {
  conversationId: number
  // @ts-ignore
  children: (conversation: Conversation) => any
}

const ConversationDataLoader: React.FC<ConversationDataLoaderProps> = ({
  conversationId,
  children,
}) => {
  const conversation = useConversationById(conversationId)
  const dispatch = useDispatch()

  useEffect(() => {
    conversation.getData()
  }, [])

  useEffect(() => {
    if (conversation.isSuccess) {
      dispatch(duckActions.setCurrentConversation(conversation.data!))
    }
  }, [conversation.data, conversation.isSuccess])

  return (
    <>
      {conversation.isPending && <p>Loading</p>}
      {conversation.isSuccess && children(conversation.data!)}
    </>
  )
}

export const Chat: React.FC<RouteComponentProps<{ id?: string }>> = ({ match }) => {
  const classes = useStyles()
  const conversationId = Number(match.params.id)
  const currentConversation = useSelector(duckSelectors.selectCurrentConversation)

  /**
   * If currentConversation is not null & conversationId === currentConversation.id => pass currentConversation to component
   * If currentConversation is null & conversationId != null => get conversationI by id & pass it to component\
   * If currentConversation is null & conversationId is null => display "Click item in sidebar"
   */

  if (currentConversation && +currentConversation.id === conversationId) {
    return <ConversationComponent conversation={currentConversation} />
  }

  if (!currentConversation && conversationId) {
    return (
      <>
        {/*
          // @ts-ignore*/}
        <ConversationDataLoader conversationId={conversationId}>
          {(conversation: Conversation) => <ConversationComponent conversation={conversation} />}
        </ConversationDataLoader>
      </>
    )
  }

  return (
    <Typography className={classes.root}>Please select the conversation in the sidebar.</Typography>
  )
}

export default withSidebar(Chat)
