import React from 'react'

import Sidebar from './sidebar'

export const withSidebar = (Component: React.ComponentType<any>) => (props: any) => {
  return (
    <>
      <Sidebar />
      <Component {...props} />
    </>
  )
}

export default {
  withSidebar,
}
