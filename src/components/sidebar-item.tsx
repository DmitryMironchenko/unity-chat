import Avatar from '@material-ui/core/Avatar'
import ListItem from '@material-ui/core/ListItem'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import ImageIcon from '@material-ui/icons/Image'
import { push } from 'connected-react-router'
import _ from 'lodash'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { ConversationWithUserData } from '../types'

import { duckOperations, duckSelectors } from './ducks'

interface Props {
  conversation: ConversationWithUserData
}

const buildMessageStatusText = (messageCount: number) =>
  messageCount < 1 ? `All caught  up` : `Unread messages: ${messageCount}`

const SidebarItem: React.FC<Props> = ({ conversation }) => {
  const currentConversation = useSelector(duckSelectors.selectCurrentConversation)
  const dispatch = useDispatch()

  const startChat = (conversation: ConversationWithUserData) => () => {
    dispatch(duckOperations.startConversation(conversation))
  }

  return (
    <ListItem
      button={true}
      selected={conversation.id === _.get(currentConversation, 'id')}
      onClick={startChat(conversation)}
    >
      <ListItemAvatar>
        <Avatar alt={conversation.with_user.username} src={conversation.with_user.avatar_url} />
      </ListItemAvatar>
      <ListItemText
        primary={conversation.with_user.username}
        secondary={buildMessageStatusText(conversation.unread_message_count)}
      />
    </ListItem>
  )
}

export default SidebarItem
