import { connectRouter, routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk'

import chatReducer, { reducerName as chatReducerName } from '../components/ducks'

export const history = createBrowserHistory()

const createRootReducer = (historrry: any) =>
  combineReducers({
    router: connectRouter(historrry),
    [chatReducerName]: chatReducer,
  })

const composeEnhancers =
  // @ts-ignore
  typeof document !== 'undefined' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose

const store = createStore(
  createRootReducer(history),
  composeEnhancers(applyMiddleware(routerMiddleware(history), thunk))
)

export default store
